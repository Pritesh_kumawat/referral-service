package com.colco.usersgroups.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets UserGroupStatus
 */
public enum UserGroupStatus {
  REJECTED("rejected"),
    PENDING("pending"),
    ACCEPTED("accepted"),
    REQUESTED("requested");

  private String value;

  UserGroupStatus(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static UserGroupStatus fromValue(String text) {
    for (UserGroupStatus b : UserGroupStatus.values()) {
      if (String.valueOf(b.value).equalsIgnoreCase(text)) {
        return b;
      }
    }
    return null;
  }
}
