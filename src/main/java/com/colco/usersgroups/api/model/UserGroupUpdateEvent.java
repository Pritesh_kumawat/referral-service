package com.colco.usersgroups.api.model;

import java.util.Objects;
import java.util.UUID;

public class UserGroupUpdateEvent {

    private final UUID groupId;
    private final long userCount;
    private final User user;

    public UserGroupUpdateEvent(UUID groupId, long userCount, User user) {
        this.groupId = groupId;
        this.userCount = userCount;
        this.user = user;
    }

    public UUID getGroupId() {
        return groupId;
    }

    public long getUserCount() {
        return userCount;
    }

    public User getUser() {
        return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserGroupUpdateEvent)) return false;
        UserGroupUpdateEvent that = (UserGroupUpdateEvent) o;
        return getGroupId().equals(that.getGroupId()) &&
                getUser().equals(that.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGroupId(), getUser());
    }

    @Override
    public String toString() {
        return "UserGroupEventModel{" +
                "groupId=" + groupId +
                ", userCount=" + userCount +
                ", user=" + user +
                '}';
    }

}
