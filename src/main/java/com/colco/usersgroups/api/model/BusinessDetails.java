package com.colco.usersgroups.api.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * BusinessDetails
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-18T04:30:28.906Z[GMT]")
public class BusinessDetails {
	@JsonProperty("legalName")
	private String legalName = null;

	@JsonProperty("docNumber")
	private String docNumber = null;

	@JsonProperty("industries")
	
	private List<String> industries = null;

	@JsonProperty("offering")
	
	private List<String> offering = null;

	@JsonProperty("myIndustry")
	private String myIndustry = null;

	@JsonProperty("yearOfEstablishment")
	private String yearOfEstablishment = null;

	@JsonProperty("contactNo")
	private String contactNo = null;

	@JsonProperty("location")
	private String location = null;


	@JsonProperty("expertise")
	
	private List<String> expertise = null;

	@JsonProperty("portFolioDetails")
	private PortFolioDetails portFolioDetails = null;

	public BusinessDetails legalName(String legalName) {
		this.legalName = legalName;
		return this;
	}

	/**
	 * Get legalName
	 * 
	 * @return legalName
	 **/
	@ApiModelProperty(required = true, value = "")
	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public BusinessDetails docNumber(String docNumber) {
		this.docNumber = docNumber;
		return this;
	}

	/**
	 * Get docNumber
	 * 
	 * @return docNumber
	 **/
	@ApiModelProperty(required = true, value = "")

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public BusinessDetails industriesServed(List<String> industries) {
		this.industries = industries;
		return this;
	}

	public String getYearOfEstablishment() {
		return yearOfEstablishment;
	}

	public void setYearOfEstablishment(String yearOfEstablishment) {
		this.yearOfEstablishment = yearOfEstablishment;
	}

	public void setIndustries(List<String> industries) {
		this.industries = industries;
	}

	public void setMyIndustry(String myIndustry) {
		this.myIndustry = myIndustry;
	}



	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public BusinessDetails addIndustriesServedItem(String industriesServedItem) {
		if (this.industries == null) {
			this.industries = new ArrayList<String>();
		}
		this.industries.add(industriesServedItem);
		return this;
	}

	/**
	 * Get industriesServed
	 * 
	 * @return industriesServed
	 **/
	@ApiModelProperty(value = "")

	public List<String> getIndustries() {
		return industries;
	}

	public void setIndustriesServed(List<String> industries) {
		this.industries = industries;
	}

	public BusinessDetails offering(List<String> offering) {
		this.offering = offering;
		return this;
	}

	public BusinessDetails addOfferingItem(String offeringItem) {
		if (this.offering == null) {
			this.offering = new ArrayList<String>();
		}
		this.offering.add(offeringItem);
		return this;
	}

	/**
	 * Get offering
	 * 
	 * @return offering
	 **/
	@ApiModelProperty(value = "")

	public List<String> getOffering() {
		return offering;
	}

	public void setOffering(List<String> offering) {
		this.offering = offering;
	}

	public BusinessDetails industry(String myIndustry) {
		this.myIndustry = myIndustry;
		return this;
	}

	/**
	 * Get industry
	 * 
	 * @return industry
	 **/
	@ApiModelProperty(value = "")

	public String getMyIndustry() {
		return myIndustry;
	}

	public void setIndustry(String myIndustry) {
		this.myIndustry = myIndustry;
	}

	public BusinessDetails expertise(List<String> expertise) {
		this.expertise = expertise;
		return this;
	}

	public BusinessDetails addExpertiseItem(String expertiseItem) {
		if (this.expertise == null) {
			this.expertise = new ArrayList<String>();
		}
		this.expertise.add(expertiseItem);
		return this;
	}

	/**
	 * Intrested list
	 * 
	 * @return expertise
	 **/
	@ApiModelProperty(value = "Intrested list")
	public List<String> getExpertise() {
		return expertise;
	}

	public void setExpertise(List<String> expertise) {
		this.expertise = expertise;
	}

	public BusinessDetails portFolioDetails(PortFolioDetails portFolioDetails) {
		this.portFolioDetails = portFolioDetails;
		return this;
	}

	/**
	 * Get portFolioDetails
	 * 
	 * @return portFolioDetails
	 **/
	@ApiModelProperty(value = "")
	public PortFolioDetails getPortFolioDetails() {
		return portFolioDetails;
	}

	public void setPortFolioDetails(PortFolioDetails portFolioDetails) {
		this.portFolioDetails = portFolioDetails;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		BusinessDetails businessDetails = (BusinessDetails) o;
		return Objects.equals(this.legalName, businessDetails.legalName)
				&& Objects.equals(this.docNumber, businessDetails.docNumber)
				&& Objects.equals(this.industries, businessDetails.industries)
				&& Objects.equals(this.offering, businessDetails.offering)
				&& Objects.equals(this.myIndustry, businessDetails.myIndustry)
				&& Objects.equals(this.expertise, businessDetails.expertise)
				&& Objects.equals(this.portFolioDetails, businessDetails.portFolioDetails);
	}

	@Override
	public int hashCode() {
		return Objects.hash(legalName, docNumber, industries, offering, myIndustry, expertise,
				portFolioDetails);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class BusinessDetails {\n");

		sb.append("    legalName: ").append(toIndentedString(legalName)).append("\n");
		sb.append("    docNumber: ").append(toIndentedString(docNumber)).append("\n");
		sb.append("    industries: ").append(toIndentedString(industries)).append("\n");
		sb.append("    offering: ").append(toIndentedString(offering)).append("\n");
		sb.append("    myIndustry: ").append(toIndentedString(myIndustry)).append("\n");
		sb.append("    expertise: ").append(toIndentedString(expertise)).append("\n");
		sb.append("    portFolioDetails: ").append(toIndentedString(portFolioDetails)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
