package com.colco.usersgroups.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum RolesEnum {
	N_USER("n_user"),
	B_USER("b_user"),
	GROUP_USER("group_user"),
	GROUP_OWNER("group_owner"),
	USER_ADMIN("user_admin");

	private String value;

	RolesEnum(String value) {
		this.value = value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static RolesEnum fromValue(String text) {
		for (RolesEnum b : RolesEnum.values()) {
			if (String.valueOf(b.value).equalsIgnoreCase(text)) {
				return b;
			}
		}
		return null;
	}
}
