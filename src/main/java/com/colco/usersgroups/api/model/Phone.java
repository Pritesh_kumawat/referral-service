package com.colco.usersgroups.api.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Validated
@ApiModel(value = "Phone", description = "Represents a phone number, parsed using the Google phonelib library")
public class Phone {
	@JsonProperty("type")
	String type = "DEFAULT";
	@JsonProperty("e164")
	String e164;
	@JsonProperty("countryCode")
	String countryCode;
	@JsonProperty("nationalNumber")
	String nationalNumber;
	@JsonProperty("extension")
	String extension;
	@JsonProperty("nationalFormat")
	String nationalFormat;
	@JsonProperty("internationalFormat")
	String internationalFormat;

	@ApiModelProperty(readOnly = true, value = "The type of the phone number from the Google phonelib library", example = "MOBILE")
	@Valid
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Phone type(String type) {
		this.type = type;
		return this;
	}

	@ApiModelProperty(readOnly = true, value = "The e164 representation of this phone number if its available. May not exist if the phone number is unparseable", example = "+18885551212")
	@Valid
	public String getE164() {
		return e164;
	}

	public void setE164(String e164) {
		this.e164 = e164;
	}

	public Phone e164(String e164) {
		this.e164 = e164;
		return this;
	}

	@ApiModelProperty(readOnly = true, value = "The IDD country calling code if its available. May not be available if the phone number is unparseable", example = "1")
	@Valid
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Phone countryCode(String countryCode) {
		this.countryCode = countryCode;
		return this;
	}


	@ApiModelProperty(readOnly = true, value = "The 'local' unformatted version of the phone number, used if the caller is in the same Locale. May not be available if the phone number is unparseable", example = "8885551212")
	@Valid
	public String getNationalNumber() {
		return nationalNumber;
	}

	public void setNationalNumber(String nationalNumber) {
		this.nationalNumber = nationalNumber;
	}

	public Phone nationalNumber(String nationalNumber) {
		this.nationalNumber = nationalNumber;
		return this;
	}


	@ApiModelProperty(readOnly = true, value = "The extension portion of the phone number", example = "345")
	@Valid
	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Phone extension(String extension) {
		this.extension = extension;
		return this;
	}


	@ApiModelProperty(readOnly = true, value = "The 'local' formatted version of the phone number, used if the caller is in the same Locale. May not be available if the phone number is unparseable", example = "(888) 555-1212\n")
	@Valid
	public String getNationalFormat() {
		return nationalFormat;
	}

	public void setNationalFormat(String nationalFormat) {
		this.nationalFormat = nationalFormat;
	}

	public Phone nationalFormat(String nationalFormat) {
		this.nationalFormat = nationalFormat;
		return this;
	}

	@ApiModelProperty(readOnly = true, value = "The international formatted version of the phone number, used if the caller is not in the same Locale. May not be available if the phone number is unparseable", example = "+1 888-555-1212")
	@Valid
	public String getInternationalFormat() {
		return internationalFormat;
	}

	public void setInternationalFormat(String internationalFormat) {
		this.internationalFormat = internationalFormat;
	}

	public Phone internationalFormat(String internationalFormat) {
		this.internationalFormat = internationalFormat;
		return this;
	}

	@Override
	public String toString() {
		return "Phone{" +
				"type='" + type + '\'' +
				", e164='" + e164 + '\'' +
				", countryCode='" + countryCode + '\'' +
				", nationalNumber='" + nationalNumber + '\'' +
				", extension='" + extension + '\'' +
				", nationalFormat='" + nationalFormat + '\'' +
				", internationalFormat='" + internationalFormat + '\'' +
				'}';
	}
}
