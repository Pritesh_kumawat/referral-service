package com.colco.usersgroups.api.model;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * Group
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-01-12T01:01:54.603+05:30[Asia/Calcutta]")
public class Group   {
	@JsonProperty("id")
	private UUID id = null;

	@JsonProperty("name")
	private String name = null;

	@JsonProperty("description")
	private String description = null;

	@JsonProperty("createdByUserId")
	private String createdByUserId = null;



	public String getCreatedByUserId() {
		return createdByUserId;
	}

	public void setCreatedByUserId(String createdByUserId) {
		this.createdByUserId = createdByUserId;
	}

	@JsonProperty("users")
	@Valid
	private List<UserGroup> users = null;

	@JsonProperty("applicationCodes")
	@Valid
	private List<String> applicationCodes = null;

	@JsonProperty("tags")
	@Valid
	private List<String> tags = null;

	public Group id(UUID id) {
		this.id = id;
		return this;
	}

	/**
	 * Get id
	 * @return id
	 **/
	@ApiModelProperty(readOnly = true, value = "")

	@Valid
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Group name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Get name
	 * @return name
	 **/
	@ApiModelProperty(example = "Committee", required = true, value = "")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Group description(String description) {
		this.description = description;
		return this;
	}

	/**
	 * Get description
	 * @return description
	 **/
	@ApiModelProperty(example = "Group for all members that are responsible for collab", required = true, value = "")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Group users(List<UserGroup> users) {
		this.users = users;
		return this;
	}

	public Group addUsersItem(UserGroup usersItem) {
		if (this.users == null) {
			this.users = new ArrayList<UserGroup>();
		}
		this.users.add(usersItem);
		return this;
	}

	/**
	 * Members of this group, with their role
	 * @return users
	 **/
	@ApiModelProperty(value = "Members of this group, with their role")
	@Valid
	public List<UserGroup> getUsers() {
		return users;
	}

	public void setUsers(List<UserGroup> users) {
		this.users = users;
	}

	public Group applicationCodes(List<String> applicationCodes) {
		this.applicationCodes = applicationCodes;
		return this;
	}

	public Group addApplicationCodesItem(String applicationCodesItem) {
		if (this.applicationCodes == null) {
			this.applicationCodes = new ArrayList<String>();
		}
		this.applicationCodes.add(applicationCodesItem);
		return this;
	}

	/**
	 * Get applicationCodes
	 * @return applicationCodes
	 **/
	@ApiModelProperty(example = "[\"collab\",\"follower\",\"community\",\"network\"]", value = "")

	public List<String> getApplicationCodes() {
		return applicationCodes;
	}

	public void setApplicationCodes(List<String> applicationCodes) {
		this.applicationCodes = applicationCodes;
	}

	public Group tags(List<String> tags) {
		this.tags = tags;
		return this;
	}

	public Group addTagsItem(String tagsItem) {
		if (this.tags == null) {
			this.tags = new ArrayList<String>();
		}
		this.tags.add(tagsItem);
		return this;
	}

	/**
	 * Get tags
	 * @return tags
	 **/
	@ApiModelProperty(example = "[\"mailinglist\",\"authoring\"]", value = "")

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}


	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Group group = (Group) o;
		return Objects.equals(this.id, group.id) &&
				Objects.equals(this.name, group.name) &&
				Objects.equals(this.description, group.description) &&
				Objects.equals(this.users, group.users) &&
				Objects.equals(this.applicationCodes, group.applicationCodes) &&
				Objects.equals(this.tags, group.tags);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, description, users, applicationCodes, tags);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Group {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    description: ").append(toIndentedString(description)).append("\n");
		sb.append("    users: ").append(toIndentedString(users)).append("\n");
		sb.append("    applicationCodes: ").append(toIndentedString(applicationCodes)).append("\n");
		sb.append("    tags: ").append(toIndentedString(tags)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
