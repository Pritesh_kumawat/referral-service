package com.colco.usersgroups.api.model;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * User
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-01-12T01:01:54.603+05:30[Asia/Calcutta]")
public class User {
	@JsonProperty("id")
	private UUID id = null;

	@JsonProperty("emailAddress")
	private String emailAddress = null;

	@JsonProperty("username")
	private String username = null;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonProperty("description")
	private String description = null;

	@JsonProperty("links")
	private List<LinksDetails> links = null;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<LinksDetails> getLinks() {
		return links;
	}

	public void setLinks(List<LinksDetails> links) {
		this.links = links;
	}

	@JsonProperty("credentialsNonExpired")
	private Boolean credentialsNonExpired = null;

	@JsonProperty("status")
	private String status = null;

	@JsonProperty("authProvider")
	private String authProvider = "local";

	@JsonProperty("isVerified")
	private Boolean isVerified;

	public String getAuthProvider() {
		return authProvider;
	}

	public void setAuthProvider(String authProvider) {
		this.authProvider = authProvider;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	@JsonProperty("fileId")
	private UUID fileId = null;

	@JsonProperty("roles")
	private RolesEnum roles = null;

	@JsonProperty("userDetails")
	private UserDetails userDetails = null;

	@JsonProperty("fcmToken")
	private String fcmToken = null;

	@JsonProperty("connected")
	private Boolean connected = false;

	private String requestStatus = null;

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	@JsonProperty("following")
	private Boolean following = false;

	@JsonProperty("businessDetails")
	private BusinessDetails businessDetails = null;

	public User id(UUID id) {
		this.id = id;
		return this;
	}

	/**
	 * The uuid id of the user, for oracle custs, this will be decimal to hex
	 * conversion and stored in a uuid
	 * 
	 * @return id
	 **/
	@ApiModelProperty(readOnly = true, value = "The uuid id of the user, for oracle custs, this will be decimal to hex conversion and stored in a uuid")

	@Valid
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Boolean getConnected() {
		return connected;
	}

	public void setConnected(Boolean connected) {
		this.connected = connected;
	}

	public Boolean getFollowing() {
		return following;
	}

	public void setFollowing(Boolean following) {
		this.following = following;
	}

	public User emailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
		return this;
	}

	/**
	 * Get emailAddress
	 * 
	 * @return emailAddress
	 **/
	@ApiModelProperty(example = "test.loblaw@lawblogger.com", required = true, value = "")
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public User credentialsNonExpired(Boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
		return this;
	}

	/**
	 * to reset password
	 * 
	 * @return credentialsNonExpired
	 **/
	@ApiModelProperty(example = "true", value = "to reset password")

	public Boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public User status(String status) {
		this.status = status;
		return this;
	}

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}

	/**
	 * Get status
	 * 
	 * @return status
	 **/
	@ApiModelProperty(example = "A", value = "")

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User fileId(UUID fileId) {
		this.fileId = fileId;
		return this;
	}

	/**
	 * Get fileId
	 * 
	 * @return fileId
	 **/
	@ApiModelProperty(value = "")

	public UUID getFileId() {
		return fileId;
	}

	public void setFileId(UUID fileId) {
		this.fileId = fileId;
	}

	public User roles(RolesEnum roles) {
		this.roles = roles;
		return this;
	}

	/**
	 * Get roles
	 * 
	 * @return roles
	 **/
	@ApiModelProperty(required = true, value = "")
	public RolesEnum getRoles() {
		return roles;
	}

	public void setRoles(RolesEnum roles) {
		this.roles = roles;
	}

	public User userDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
		return this;
	}

	/**
	 * Get userDetails
	 * 
	 * @return userDetails
	 **/
	@ApiModelProperty(value = "")

	@Valid
	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public User businessDetails(BusinessDetails businessDetails) {
		this.businessDetails = businessDetails;
		return this;
	}

	/**
	 * Get businessDetails
	 * 
	 * @return businessDetails
	 **/
	@ApiModelProperty(value = "")

	@Valid
	public BusinessDetails getBusinessDetails() {
		return businessDetails;
	}

	public void setBusinessDetails(BusinessDetails businessDetails) {
		this.businessDetails = businessDetails;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		User user = (User) o;
		return Objects.equals(this.id, user.id) && Objects.equals(this.emailAddress, user.emailAddress)
				&& Objects.equals(this.credentialsNonExpired, user.credentialsNonExpired)
				&& Objects.equals(this.status, user.status) && Objects.equals(this.fileId, user.fileId)
				&& Objects.equals(this.roles, user.roles) && Objects.equals(this.userDetails, user.userDetails)
				&& Objects.equals(this.businessDetails, user.businessDetails);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, emailAddress, credentialsNonExpired, status, fileId, roles, userDetails,
				businessDetails);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class User {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    emailAddress: ").append(toIndentedString(emailAddress)).append("\n");
		sb.append("    credentialsNonExpired: ").append(toIndentedString(credentialsNonExpired)).append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    fileId: ").append(toIndentedString(fileId)).append("\n");
		sb.append("    roles: ").append(toIndentedString(roles)).append("\n");
		sb.append("    userDetails: ").append(toIndentedString(userDetails)).append("\n");
		sb.append("    businessDetails: ").append(toIndentedString(businessDetails)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
