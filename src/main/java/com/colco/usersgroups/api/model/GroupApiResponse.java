package com.colco.usersgroups.api.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.colco.referralservice.api.model.Pagination;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Represents a response to a operation on the Group object.
 */
@ApiModel(description = "Represents a response to a operation on the Group object.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-01-12T01:01:54.603+05:30[Asia/Calcutta]")
public class GroupApiResponse   {
  @JsonProperty("statusCode")
  private Integer statusCode = null;

  @JsonProperty("statusMessage")
  private String statusMessage = null;

  @JsonProperty("pagination")
  private Pagination pagination = null;

  @JsonProperty("results")
  @Valid
  private List<Group> results = new ArrayList<Group>();

  @JsonProperty("errorMessages")
  @Valid
  private List<String> errorMessages = null;

  @JsonProperty("messages")
  @Valid
  private List<String> messages = null;

  public GroupApiResponse statusCode(Integer statusCode) {
    this.statusCode = statusCode;
    return this;
  }

  /**
   * Get statusCode
   * @return statusCode
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  public Integer getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(Integer statusCode) {
    this.statusCode = statusCode;
  }

  public GroupApiResponse statusMessage(String statusMessage) {
    this.statusMessage = statusMessage;
    return this;
  }

  /**
   * Get statusMessage
   * @return statusMessage
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  public String getStatusMessage() {
    return statusMessage;
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = statusMessage;
  }

  public GroupApiResponse pagination(Pagination pagination) {
    this.pagination = pagination;
    return this;
  }

  /**
   * Get pagination
   * @return pagination
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
  public Pagination getPagination() {
    return pagination;
  }

  public void setPagination(Pagination pagination) {
    this.pagination = pagination;
  }

  public GroupApiResponse results(List<Group> results) {
    this.results = results;
    return this;
  }

  public GroupApiResponse addResultsItem(Group resultsItem) {
    this.results.add(resultsItem);
    return this;
  }

  /**
   * Get results
   * @return results
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  @Valid
  public List<Group> getResults() {
    return results;
  }

  public void setResults(List<Group> results) {
    this.results = results;
  }

  public GroupApiResponse errorMessages(List<String> errorMessages) {
    this.errorMessages = errorMessages;
    return this;
  }

  public GroupApiResponse addErrorMessagesItem(String errorMessagesItem) {
    if (this.errorMessages == null) {
      this.errorMessages = new ArrayList<String>();
    }
    this.errorMessages.add(errorMessagesItem);
    return this;
  }

  /**
   * An optional array of error messages providing more detail about the results
   * @return errorMessages
  **/
  @ApiModelProperty(value = "An optional array of error messages providing more detail about the results")

  public List<String> getErrorMessages() {
    return errorMessages;
  }

  public void setErrorMessages(List<String> errorMessages) {
    this.errorMessages = errorMessages;
  }

  public GroupApiResponse messages(List<String> messages) {
    this.messages = messages;
    return this;
  }

  public GroupApiResponse addMessagesItem(String messagesItem) {
    if (this.messages == null) {
      this.messages = new ArrayList<String>();
    }
    this.messages.add(messagesItem);
    return this;
  }

  /**
   * An optional array of messages providing more detail about the results
   * @return messages
  **/
  @ApiModelProperty(value = "An optional array of messages providing more detail about the results")

  public List<String> getMessages() {
    return messages;
  }

  public void setMessages(List<String> messages) {
    this.messages = messages;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GroupApiResponse groupApiResponse = (GroupApiResponse) o;
    return Objects.equals(this.statusCode, groupApiResponse.statusCode) &&
        Objects.equals(this.statusMessage, groupApiResponse.statusMessage) &&
        Objects.equals(this.pagination, groupApiResponse.pagination) &&
        Objects.equals(this.results, groupApiResponse.results) &&
        Objects.equals(this.errorMessages, groupApiResponse.errorMessages) &&
        Objects.equals(this.messages, groupApiResponse.messages);
  }

  @Override
  public int hashCode() {
    return Objects.hash(statusCode, statusMessage, pagination, results, errorMessages, messages);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GroupApiResponse {\n");
    
    sb.append("    statusCode: ").append(toIndentedString(statusCode)).append("\n");
    sb.append("    statusMessage: ").append(toIndentedString(statusMessage)).append("\n");
    sb.append("    pagination: ").append(toIndentedString(pagination)).append("\n");
    sb.append("    results: ").append(toIndentedString(results)).append("\n");
    sb.append("    errorMessages: ").append(toIndentedString(errorMessages)).append("\n");
    sb.append("    messages: ").append(toIndentedString(messages)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
