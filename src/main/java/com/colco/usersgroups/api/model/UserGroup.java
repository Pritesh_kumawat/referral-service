package com.colco.usersgroups.api.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * UserGroup
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T07:40:57.720Z[GMT]")
public class UserGroup   {
	 @JsonProperty("userId")
	  private UUID userId = null;

	  @JsonProperty("displayName")
	  private String displayName = null;
	  
	  @JsonProperty("fileId")
	  private UUID fileId = null;

	  public UUID getFileId() {
		return fileId;
	}

	public void setFileId(UUID fileId) {
		this.fileId = fileId;
	}

	@JsonProperty("status")
	  private UserGroupStatus status = null;

	  @JsonProperty("role")
	  @Valid
	  private List<String> role = new ArrayList<String>();

	  public UserGroup userId(UUID userId) {
	    this.userId = userId;
	    return this;
	  }

	  /**
	   * Get userId
	   * @return userId
	  **/
	  @ApiModelProperty(required = true, value = "")
	      @NotNull

	    @Valid
	    public UUID getUserId() {
	    return userId;
	  }

	  public void setUserId(UUID userId) {
	    this.userId = userId;
	  }

	  public UserGroup displayName(String displayName) {
	    this.displayName = displayName;
	    return this;
	  }

	  /**
	   * Get displayName
	   * @return displayName
	  **/
	  @ApiModelProperty(example = "Robert Loblaw", required = true, value = "")
	      @NotNull

	    public String getDisplayName() {
	    return displayName;
	  }

	  public void setDisplayName(String displayName) {
	    this.displayName = displayName;
	  }

	  public UserGroup status(UserGroupStatus status) {
	    this.status = status;
	    return this;
	  }

	  /**
	   * Get status
	   * @return status
	  **/
	  @ApiModelProperty(value = "")
	  
	    @Valid
	    public UserGroupStatus getStatus() {
	    return status;
	  }

	  public void setStatus(UserGroupStatus status) {
	    this.status = status;
	  }

	  public UserGroup role(List<String> role) {
	    this.role = role;
	    return this;
	  }

	  public UserGroup addRoleItem(String roleItem) {
	    this.role.add(roleItem);
	    return this;
	  }

	  /**
	   * Get role
	   * @return role
	  **/
	  @ApiModelProperty(example = "[\"collabe\",\"collabrator\"]", required = true, value = "")
	      @NotNull

	    public List<String> getRole() {
	    return role;
	  }

	  public void setRole(List<String> role) {
	    this.role = role;
	  }


	  @Override
	  public boolean equals(java.lang.Object o) {
	    if (this == o) {
	      return true;
	    }
	    if (o == null || getClass() != o.getClass()) {
	      return false;
	    }
	    UserGroup userGroup = (UserGroup) o;
	    return Objects.equals(this.userId, userGroup.userId) &&
	        Objects.equals(this.displayName, userGroup.displayName) &&
	        Objects.equals(this.status, userGroup.status) &&
	        Objects.equals(this.role, userGroup.role);
	  }

	  @Override
	  public int hashCode() {
	    return Objects.hash(userId, displayName, status, role);
	  }

	  @Override
	  public String toString() {
	    StringBuilder sb = new StringBuilder();
	    sb.append("class UserGroup {\n");
	    
	    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
	    sb.append("    displayName: ").append(toIndentedString(displayName)).append("\n");
	    sb.append("    status: ").append(toIndentedString(status)).append("\n");
	    
	    sb.append("    fileId: ").append(toIndentedString(fileId)).append("\n");
	    sb.append("    role: ").append(toIndentedString(role)).append("\n");
	    sb.append("}");
	    return sb.toString();
	  }

	  /**
	   * Convert the given object to string with each line indented by 4 spaces
	   * (except the first line).
	   */
	  private String toIndentedString(java.lang.Object o) {
	    if (o == null) {
	      return "null";
	    }
	    return o.toString().replace("\n", "\n    ");
	  }
}
