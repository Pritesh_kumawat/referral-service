package com.colco.usersgroups.api.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Intrested list
 */
@ApiModel(description = "Intrested list")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-18T04:30:28.906Z[GMT]")
public class UserDetailsInterest   {
  @JsonProperty("professional")
  @Valid
  private List<String> professional = null;

  @JsonProperty("collab")
  @Valid
  private List<String> collab = null;

  public UserDetailsInterest professional(List<String> professional) {
    this.professional = professional;
    return this;
  }

  public UserDetailsInterest addProfessionalItem(String professionalItem) {
    if (this.professional == null) {
      this.professional = new ArrayList<String>();
    }
    this.professional.add(professionalItem);
    return this;
  }

  /**
   * Get professional
   * @return professional
  **/
  @ApiModelProperty(value = "")
  
    public List<String> getProfessional() {
    return professional;
  }

  public void setProfessional(List<String> professional) {
    this.professional = professional;
  }

  public UserDetailsInterest collab(List<String> collab) {
    this.collab = collab;
    return this;
  }

  public UserDetailsInterest addCollabItem(String collabItem) {
    if (this.collab == null) {
      this.collab = new ArrayList<String>();
    }
    this.collab.add(collabItem);
    return this;
  }

  /**
   * Get collab
   * @return collab
  **/
  @ApiModelProperty(value = "")
  
    public List<String> getCollab() {
    return collab;
  }

  public void setCollab(List<String> collab) {
    this.collab = collab;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserDetailsInterest userDetailsInterest = (UserDetailsInterest) o;
    return Objects.equals(this.professional, userDetailsInterest.professional) &&
        Objects.equals(this.collab, userDetailsInterest.collab);
  }

  @Override
  public int hashCode() {
    return Objects.hash(professional, collab);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserDetailsInterest {\n");
    
    sb.append("    professional: ").append(toIndentedString(professional)).append("\n");
    sb.append("    collab: ").append(toIndentedString(collab)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
