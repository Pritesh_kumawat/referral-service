package com.colco.usersgroups.api.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

/**
 * UserDetails
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-18T04:30:28.906Z[GMT]")
public class UserDetails {

	@JsonProperty("firstName")

	private String firstName = null;

	@JsonProperty("lastName")
	private String lastName = null;

	@JsonProperty("middleName")

	private String middleName = null;

	@JsonProperty("dateOfBirth")
	private String dateOfBirth;

	@JsonProperty("contactNo")
	private String contactNo = null;

	@JsonProperty("location")
	private String location = null;

	@JsonProperty("credentialsNonExpired")
	private Boolean credentialsNonExpired = null;

	@JsonProperty("status")
	private String status = null;

	@JsonProperty("imageURL")
	private String imageURL = null;

	/**
	 * Gets or Sets gender
	 */
	public enum GenderEnum {
		MALE("Male"),

		FEMALE("Female");

		private String value;

		GenderEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static GenderEnum fromValue(String text) {
			for (GenderEnum b : GenderEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}

	@JsonProperty("gender")
	private GenderEnum gender = null;

	/**
	 * Gets or Sets roles
	 */
	public enum RolesEnum {
		USER("user"),

		ADMIN("admin"),

		GUEST("guest");

		private String value;

		RolesEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static RolesEnum fromValue(String text) {
			for (RolesEnum b : RolesEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}

	@JsonProperty("roles")
	private RolesEnum roles = null;

	@JsonProperty("portFolioDetails")
	private PortFolioDetails portFolioDetails = null;

	@JsonProperty("industries")
	@Valid
	private List<String> industries = null;

	@JsonProperty("expertise")
	@Valid
	private List<String> expertise = null;

	@JsonProperty("offering")
	@Valid

	private List<String> offering = null;

	@JsonProperty("interest")
	private UserDetailsInterest interest = null;

	public UserDetails firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setIndustries(List<String> industries) {
		this.industries = industries;
	}

	public void setExpertise(List<String> expertise) {
		this.expertise = expertise;
	}

	/**
	 * Get firstName
	 * 
	 * @return firstName
	 **/
	@ApiModelProperty(example = "Bob", value = "")

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public UserDetails lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	/**
	 * Get lastName
	 * 
	 * @return lastName
	 **/
	@ApiModelProperty(example = "Robert", value = "")

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public UserDetails middleName(String middleName) {
		this.middleName = middleName;
		return this;
	}

	/**
	 * Get middleName
	 * 
	 * @return middleName
	 **/
	@ApiModelProperty(example = "Blah", value = "")

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public UserDetails contactNo(String contactNo) {
		this.contactNo = contactNo;
		return this;
	}

	/**
	 * Get contactNo
	 * 
	 * @return contactNo
	 **/
	@ApiModelProperty(value = "")

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public UserDetails location(String location) {
		this.location = location;
		return this;
	}

	/**
	 * Get location
	 * 
	 * @return location
	 **/
	@ApiModelProperty(value = "")

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public UserDetails credentialsNonExpired(Boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
		return this;
	}

	/**
	 * to reset password
	 * 
	 * @return credentialsNonExpired
	 **/
	@ApiModelProperty(example = "true", value = "to reset password")

	public Boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public UserDetails status(String status) {
		this.status = status;
		return this;
	}

	/**
	 * Get status
	 * 
	 * @return status
	 **/
	@ApiModelProperty(example = "A", value = "")

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public UserDetails imageURL(String imageURL) {
		this.imageURL = imageURL;
		return this;
	}

	/**
	 * Get imageURL
	 * 
	 * @return imageURL
	 **/
	@ApiModelProperty(value = "")

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public UserDetails gender(GenderEnum gender) {
		this.gender = gender;
		return this;
	}

	/**
	 * Get gender
	 * 
	 * @return gender
	 **/
	@ApiModelProperty(value = "")

	public GenderEnum getGender() {
		return gender;
	}

	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}

	public UserDetails roles(RolesEnum roles) {
		this.roles = roles;
		return this;
	}

	/**
	 * Get roles
	 * 
	 * @return roles
	 **/
	@ApiModelProperty(value = "")

	public RolesEnum getRoles() {
		return roles;
	}

	public void setRoles(RolesEnum roles) {
		this.roles = roles;
	}

	public UserDetails portFolioDetails(PortFolioDetails portFolioDetails) {
		this.portFolioDetails = portFolioDetails;
		return this;
	}

	/**
	 * Get portFolioDetails
	 * 
	 * @return portFolioDetails
	 **/
	@ApiModelProperty(value = "")

	@Valid
	public PortFolioDetails getPortFolioDetails() {
		return portFolioDetails;
	}

	public void setPortFolioDetails(PortFolioDetails portFolioDetails) {
		this.portFolioDetails = portFolioDetails;
	}

	public UserDetails industries(List<String> industries) {
		this.industries = industries;
		return this;
	}

	public UserDetails addIndustryItem(String industryItem) {
		if (this.industries == null) {
			this.industries = new ArrayList<String>();
		}
		this.industries.add(industryItem);
		return this;
	}

	/**
	 * Get industry
	 * 
	 * @return industry
	 **/
	@ApiModelProperty(value = "")

	public List<String> getIndustries() {
		return industries;
	}

	public void setIndustry(List<String> industries) {
		this.industries = industries;
	}

	public UserDetails expertiseOn(List<String> expertise) {
		this.expertise = expertise;
		return this;
	}

	public UserDetails addExpertiseOnItem(String expertiseItem) {
		if (this.expertise == null) {
			this.expertise = new ArrayList<String>();
		}
		this.expertise.add(expertiseItem);
		return this;
	}

	/**
	 * Get expertiseOn
	 * 
	 * @return expertiseOn
	 **/
	@ApiModelProperty(value = "")

	public List<String> getExpertise() {
		return expertise;
	}

	public void setExpertiseOn(List<String> expertise) {
		this.expertise = expertise;
	}

	public UserDetails offering(List<String> offering) {
		this.offering = offering;
		return this;
	}

	public UserDetails addOfferingItem(String offeringItem) {
		if (this.offering == null) {
			this.offering = new ArrayList<String>();
		}
		this.offering.add(offeringItem);
		return this;
	}

	/**
	 * Get offering
	 * 
	 * @return offering
	 **/
	@ApiModelProperty(value = "")

	public List<String> getOffering() {
		return offering;
	}

	public void setOffering(List<String> offering) {
		this.offering = offering;
	}

	public UserDetails interest(UserDetailsInterest interest) {
		this.interest = interest;
		return this;
	}

	/**
	 * Get interest
	 * 
	 * @return interest
	 **/
	@ApiModelProperty(value = "")

	@Valid
	public UserDetailsInterest getInterest() {
		return interest;
	}

	public void setInterest(UserDetailsInterest interest) {
		this.interest = interest;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		UserDetails userDetails = (UserDetails) o;
		return Objects.equals(this.firstName, userDetails.firstName)
				&& Objects.equals(this.lastName, userDetails.lastName)
				&& Objects.equals(this.middleName, userDetails.middleName)
				&& Objects.equals(this.contactNo, userDetails.contactNo)
				&& Objects.equals(this.location, userDetails.location)
				&& Objects.equals(this.credentialsNonExpired, userDetails.credentialsNonExpired)
				&& Objects.equals(this.status, userDetails.status)
				&& Objects.equals(this.imageURL, userDetails.imageURL)
				&& Objects.equals(this.gender, userDetails.gender) && Objects.equals(this.roles, userDetails.roles)
				&& Objects.equals(this.portFolioDetails, userDetails.portFolioDetails)
				&& Objects.equals(this.industries, userDetails.industries)
				&& Objects.equals(this.expertise, userDetails.expertise)
				&& Objects.equals(this.offering, userDetails.offering)
				&& Objects.equals(this.interest, userDetails.interest);
	}

	@Override
	public int hashCode() {
		return Objects.hash(firstName, lastName, middleName, contactNo, location, credentialsNonExpired, status,
				imageURL, gender, roles, portFolioDetails, industries, expertise, offering, interest);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class UserDetails {\n");

		sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
		sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
		sb.append("    middleName: ").append(toIndentedString(middleName)).append("\n");
		sb.append("    contactNo: ").append(toIndentedString(contactNo)).append("\n");
		sb.append("    location: ").append(toIndentedString(location)).append("\n");
		sb.append("    credentialsNonExpired: ").append(toIndentedString(credentialsNonExpired)).append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    imageURL: ").append(toIndentedString(imageURL)).append("\n");
		sb.append("    gender: ").append(toIndentedString(gender)).append("\n");
		sb.append("    roles: ").append(toIndentedString(roles)).append("\n");
		sb.append("    portFolioDetails: ").append(toIndentedString(portFolioDetails)).append("\n");
		sb.append("    industries: ").append(toIndentedString(industries)).append("\n");
		sb.append("    expertiseOn: ").append(toIndentedString(expertise)).append("\n");
		sb.append("    offering: ").append(toIndentedString(offering)).append("\n");
		sb.append("    interest: ").append(toIndentedString(interest)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
