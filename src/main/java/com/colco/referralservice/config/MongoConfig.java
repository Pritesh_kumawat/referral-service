package com.colco.referralservice.config;

import static org.bson.codecs.configuration.CodecRegistries.fromCodecs;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bson.UuidRepresentation;
import org.bson.codecs.UuidCodec;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.CustomConversions;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import com.colco.referralservice.utils.OffsetDateTimeReadConverter;
import com.colco.referralservice.utils.OffsetDateTimeWriteConverter;
import com.colco.referralservice.utils.ZonedDateTimeReadConverter;
import com.colco.referralservice.utils.ZonedDateTimeWriteConverter;
import com.colco.usersgroups.api.model.RolesEnum;
import com.colco.usersgroups.api.model.UserDetails.GenderEnum;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

@ConditionalOnProperty(name = "spring.data.mongodb.host")
@Configuration
@EnableMongoAuditing
public class MongoConfig extends AbstractMongoConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(MongoConfig.class);

	public MongoConfig() {
	}

	@Value("${spring.data.mongodb.host}")
	private String mongoHosts;

	@Value("${spring.data.mongodb.username}")
	private String mongoUsername;
	@Value("${spring.data.mongodb.password}")
	private char[] mongoPassword;

	@Value("${spring.data.mongodb.database}")
	private String database;
	@Value("${spring.data.mongodb.replicaSet}")
	private String mongoReplicaSet;

	@Bean
	@Override
	public com.mongodb.MongoClient mongoClient() {
		MongoClientOptions.Builder mcoBuilder = MongoClientOptions.builder();
		CodecRegistry codecRegistry = CodecRegistries.fromRegistries(
				fromCodecs(new UuidCodec(UuidRepresentation.STANDARD)), MongoClientSettings.getDefaultCodecRegistry());
		mcoBuilder.codecRegistry(codecRegistry);
		if (mongoReplicaSet != null && !mongoReplicaSet.trim().isEmpty()) {
			mcoBuilder.requiredReplicaSetName(mongoReplicaSet);
		}
		// MongoClientURI clientURI=new MongoClientURI(mongoUri);
		MongoCredential credz = MongoCredential.createScramSha256Credential(mongoUsername, "admin", mongoPassword);
		List<ServerAddress> hosts = Arrays.stream(mongoHosts.split(","))
				.map(host -> new ServerAddress(host.split(":")[0], Integer.valueOf(host.split(":")[1])))
				.collect(Collectors.toList());
		logger.debug("Mongo connecting to :{} with username '{}'", hosts, mongoUsername);

		return new MongoClient(hosts, credz, mcoBuilder.build());
	}

	@Bean
	public MongoDbFactory mongoDbFactory() {
		return new SimpleMongoDbFactory(mongoClient(), database);
	}

	@Bean
	public MongoTemplate mongoTemplate() {
		return new MongoTemplate(mongoDbFactory(), mappingMongoConverter());
	}

	@Bean
	@Override
	public CustomConversions customConversions() {
		List<Converter> converters = new ArrayList<>();
		converters.add(new ZonedDateTimeWriteConverter());
		converters.add(new ZonedDateTimeReadConverter());
		converters.add(new OffsetDateTimeWriteConverter());
		converters.add(new OffsetDateTimeReadConverter());
		converters.add(new GenderConverter());
		converters.add(new RolesEnumConverter());
		converters.add(new UserRolesEnumConverter());
		return new MongoCustomConversions(converters);
	}

	@Bean
	@Override
	public MappingMongoConverter mappingMongoConverter() {
		MongoMappingContext mappingContext = new MongoMappingContext();
		DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory());
		MappingMongoConverter mongoConverter = new MappingMongoConverter(dbRefResolver, mappingContext);
		mongoConverter.setCustomConversions(customConversions());
		// This is needed to prevent "_class":"org.colco.name.of.class" being inserted
		// mongoConverter.setTypeMapper(new DefaultMongoTypeMapper(null));
		return mongoConverter;
	}

	@ReadingConverter
	public class GenderConverter implements Converter<String, GenderEnum> {

		@Override
		public GenderEnum convert(final String source) {
			return source == null ? null : GenderEnum.fromValue(source);
		}
	}

	@ReadingConverter
	public class RolesEnumConverter implements Converter<String, RolesEnum> {

		@Override
		public RolesEnum convert(final String source) {
			return source == null ? null : RolesEnum.fromValue(source);
		}
	}

	@ReadingConverter
	public class UserRolesEnumConverter implements Converter<String,RolesEnum> {

		@Override
		public RolesEnum convert(final String source) {
			return source == null ? null : RolesEnum.fromValue(source);
		}
	}

	@ReadingConverter
	public static class EnumConverter<T extends Enum<T>> {

		Class<T> type;

		public EnumConverter(Class<T> type) {
			this.type = type;
		}

		public Enum<T> convert(String text) {
			for (Enum<T> candidate : type.getEnumConstants()) {
				if (candidate.name().equalsIgnoreCase(text)) {
					return candidate;
				}
			}
			return null;
		}
	}

	@Bean
	public ObjectMapper mapper() {
		ObjectMapper mapper = new ObjectMapper().registerModule(new ParameterNamesModule())
				.registerModule(new Jdk8Module()).registerModule(new JavaTimeModule());
		mapper.setDateFormat(new StdDateFormat());
		SimpleModule simpleModule = new SimpleModule();
		simpleModule.addSerializer(OffsetDateTime.class, new JsonSerializer<OffsetDateTime>() {
			@Override
			public void serialize(OffsetDateTime offsetDateTime, JsonGenerator jsonGenerator,
					SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
				jsonGenerator.writeString(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(offsetDateTime));
			}
		});
		mapper.registerModule(simpleModule);
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		// mapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, false);
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper;

	}

	@Override
	protected String getDatabaseName() {
		// TODO Auto-generated method stub
		return mongoDbFactory().getDb().getName();
	}
}
