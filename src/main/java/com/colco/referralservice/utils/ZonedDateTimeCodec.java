package com.colco.referralservice.utils;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * Decoder/Encoder for ZonedDateTime instances in Mongo
 */
public class ZonedDateTimeCodec implements Codec<ZonedDateTime> {
    @Override
    public ZonedDateTime decode(BsonReader bsonReader, DecoderContext decoderContext) {
        long dateTime = bsonReader.readDateTime();
        Instant i = Instant.ofEpochSecond(dateTime);
        return i.atZone(ZoneOffset.UTC);
    }

    @Override
    public void encode(BsonWriter bsonWriter, ZonedDateTime zonedDateTime, EncoderContext encoderContext) {
        if(zonedDateTime == null){
            bsonWriter.writeNull();
        }else{
            ZonedDateTime utcZdt = zonedDateTime.withZoneSameInstant(ZoneOffset.UTC);
            Instant i = utcZdt.toInstant();
            bsonWriter.writeDateTime(i.toEpochMilli());
        }
    }

    @Override
    public Class<ZonedDateTime> getEncoderClass() {
        return ZonedDateTime.class;
    }
}
