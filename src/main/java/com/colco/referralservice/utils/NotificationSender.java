package com.colco.referralservice.utils;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class NotificationSender {

	Logger logger = LoggerFactory.getLogger(NotificationSender.class);


	@Value("${my.fcm.key}")
	private String fcmKey;
	@Autowired
	ObjectMapper mapper;

	private static final String FCM_API = "https://fcm.googleapis.com/fcm/send";

	/**
	 * 
	 * @param push
	 * @return
	 * @throws JsonProcessingException 
	 */

	public FirebaseResponse sendNotification(Object push) throws JsonProcessingException {
		
		
		String input = mapper.writeValueAsString(push);
		logger.info("In sendNotification() notification object {}",input);
		HttpEntity<String> request = new HttpEntity<>(input.toString());
		CompletableFuture<FirebaseResponse> pushNotification = this.send(request);
		CompletableFuture.allOf(pushNotification).join();

		FirebaseResponse firebaseResponse = null;

		try {
			firebaseResponse = pushNotification.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return firebaseResponse;
	}

	/**
	 * send push notification to API FCM
	 * 
	 * Using CompletableFuture with @Async to provide Asynchronous call.
	 * 
	 * 
	 * @param entity
	 * @return
	 */
	
	private CompletableFuture<FirebaseResponse> send(HttpEntity<String> entity) {

		RestTemplate restTemplate = new RestTemplate();
		logger.info("In CompletableFuture send() notification object "+entity.getBody());
		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + fcmKey));
		interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
		restTemplate.setInterceptors(interceptors);

		FirebaseResponse firebaseResponse = restTemplate.postForObject(FCM_API, entity, FirebaseResponse.class);

		return CompletableFuture.completedFuture(firebaseResponse);
	}
	
	
	
}
