package com.colco.referralservice.utils;

import java.net.URL;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.colco.referralservice.api.model.CreatedBy;
import com.colco.usersgroups.api.model.RolesEnum;
import com.colco.usersgroups.api.model.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Shrish
 *
 */
@Component
public class CommonUttils<T> {

	public static <T> T loadJson(String resource, TypeReference<T> typeReference) {
		try {
			URL uri = ClassPathResource.class.getResource(resource + ".json");
			return new ObjectMapper().readValue(uri, typeReference);
		} catch (Exception e) {
			logger.error("failed to reade file resource file {}  due to", resource + ".json", e);

		}
		return null;
	}

	private static final Logger logger = LoggerFactory.getLogger(CommonUttils.class);
	@Autowired
	ObjectMapper objectMapper;

	/**
	 * to add zeros to the left side of the string- if string length is less then
	 * minimum length(paddingLen) required the append zeros to the left of the
	 * string
	 * 
	 * @param numberString string (string of number) which need to be padded with
	 *                     zero's
	 * @param paddingLen   minimum length required for the string, if string length
	 *                     is less then paddingLen then append zeros to the left
	 *                     side of the string
	 * @return
	 */
	public static String leftZeroPadding(String numberString, Integer paddingLen) {
		if (numberString.length() < paddingLen) {

			return org.apache.commons.lang3.StringUtils.leftPad(numberString, paddingLen, "0");
		}
		return numberString;
	}

	public static String formUriText(String str) {
		return str.replaceAll("\\s{2,}", " ").trim().replaceAll("[^\\w ]", "").toLowerCase().replaceAll("\\s+", "-");

	}

	/**
	 * check the given string is uuid or not
	 * 
	 * @return true if the given string is UUID else false
	 */
	public static Boolean isUUID(String uuid) {

		try {
			UUID.fromString(uuid);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public static boolean isStringNullOrEmpty(String str) {
		if (str != null && !str.trim().isEmpty())
			return false;
		return true;
	}

	public static boolean contains(String obj, List<String> strings) {

		for (String string : strings) {
			if (obj.equalsIgnoreCase(string)) {
				return true;
			}
		}
		return false;
	}

	public static CreatedBy getCreatedBy(User user) {
		CreatedBy createdBy = new CreatedBy();
		createdBy.setFileId(user.getFileId());
		createdBy.setIndustries(getIndustries(user));
		createdBy.setName(getDisplayName(user));
		createdBy.setOfferings(getOfferings(user));
		createdBy.setUserId(user.getId());
		return createdBy;
	}

	public static String getDisplayName(User user) {
		if (user == null) {
			return null;
		}
		if (RolesEnum.N_USER.equals(user.getRoles()) && user.getUserDetails() != null) {
			return concatStrings(user.getUserDetails().getFirstName(), user.getUserDetails().getLastName());
		} else if (RolesEnum.B_USER.equals(user.getRoles()) && user.getBusinessDetails() != null) {
			return concatStrings(user.getBusinessDetails().getLegalName(), null);
		}
		return null;

	}

	public static List<String> getIndustries(User user) {

		if (RolesEnum.N_USER.equals(user.getRoles())) {
			return user.getUserDetails() != null ? user.getUserDetails().getIndustries() : null;
		} else if (RolesEnum.B_USER.equals(user.getRoles())) {
			return user.getBusinessDetails() != null ? user.getBusinessDetails().getIndustries() : null;
		} else {
			return null;
		}

	}

	public static List<String> getOfferings(User user) {

		if (RolesEnum.N_USER.equals(user.getRoles())) {
			return user.getUserDetails() != null ? user.getUserDetails().getOffering() : null;
		} else if (RolesEnum.B_USER.equals(user.getRoles())) {
			return user.getBusinessDetails() != null ? user.getBusinessDetails().getOffering() : null;
		} else {
			return null;
		}

	}

	public static String concatStrings(String firstPart, String secondPart) {
		StringBuilder builder = new StringBuilder();
		if (!isStringNullOrEmpty(firstPart)) {
			builder.append(firstPart.trim()).append(" ");
		}

		if (!isStringNullOrEmpty(secondPart)) {
			builder.append(secondPart.trim());
		}
		if (builder.length() > 0) {
			return builder.toString().trim();
		}
		return null;
	}

}
