package com.colco.referralservice.utils;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;


/**
 * Decoder/Encoder for OffsetDateTime instances in Mongo
 */
public class OffsetDateTimeCodec implements Codec<OffsetDateTime> {

    @Override
    public void encode(final BsonWriter writer, final OffsetDateTime offsetDateTime, final EncoderContext encoderContext) {
        if(offsetDateTime == null){
            writer.writeNull();
        }else{
            OffsetDateTime utcZdt = offsetDateTime.withOffsetSameInstant(ZoneOffset.UTC);
            Instant i = utcZdt.toInstant();
            writer.writeDateTime(i.toEpochMilli());
        }
    }

    @Override
    public OffsetDateTime decode(final BsonReader reader, final DecoderContext decoderContext) {
        long dateTime = reader.readDateTime();
        Instant i = Instant.ofEpochSecond(dateTime);
        return i.atOffset(ZoneOffset.UTC);
    }

    @Override
    public Class<OffsetDateTime> getEncoderClass() {
        return OffsetDateTime.class;
    }
}