package com.colco.referralservice.utils;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class is used to convert a class annotated with the org.springframework.data.mongodb.core.mapping.Document annotation
 * into a map of properties that can be used for a PATCH.
 */
@Component
public class MongoUpdateUtil {
	private static final Logger logger = LoggerFactory.getLogger(MongoUpdateUtil.class);
	private MongoUpdateUtil(){}
	private enum UpdateMethod {
		PATCH,PUT;
	}

	@Autowired ObjectMapper mapper;
	/**
	 * Take in a object that has the org.springframework.data.mongodb.core.mapping.Document annotation, and return a
	 * org.springframework.data.mongodb.core.query.Update that only has the values that are set
	 * @param doc The document to convert
	 * @return An Update object that contains only the non-null fields of the document
	 */
	public Update convertModelForPatch(Object doc){
		return convertModel(doc,UpdateMethod.PATCH);
	}

	/**
	 * Take in a object that has the org.springframework.data.mongodb.core.mapping.Document annotation, and return a
	 * org.springframework.data.mongodb.core.query.Update that only has the values that are set
	 * @param doc The document to convert
	 * @return An Update object that contains only the non-null fields of the document
	 */
	public Update convertModelForPut(Object doc){
		return convertModel(doc,UpdateMethod.PUT);
	}

	private Update convertModel(Object doc, UpdateMethod updateMethod){
		Update update = new Update();

		Document mdbAnnotation = doc.getClass().getAnnotation(Document.class);
		if(mdbAnnotation == null) {
			throw new IllegalArgumentException("Given object does not have the spring data Document annotation");
		}

		try {

			//			ObjectMapper mapper=new ObjectMapper();
			//			String obj=mapper.writeValueAsString(doc);
			Map<String,Object> objectMap=mapper.convertValue(doc, new TypeReference<Map<String,Object>>(){});
			//			//Loop through all of the public properties
			//			Map<String,Object> objectMap = Arrays.stream(
			//					Introspector.getBeanInfo(doc.getClass(), Object.class).getPropertyDescriptors()
			//					)
			//					//We only want properties with getters
			//					.filter(pd -> Objects.nonNull(pd.getReadMethod()))
			//					//Filter out properties that are null or inaccessible
			//					.filter(pd -> {
			//						try {
			//							return pd.getReadMethod().invoke(doc) != null;
			//						} catch (IllegalAccessException | InvocationTargetException e) {
			//							return false;
			//						}
			//					})
			//					//Create map with key=getter name, value = getter return
			//					.collect(Collectors.toMap(
			//							//Build name using Jackson JsonProperty annotation if its there, otherwise bean naming
			//							pd->{
			//								JsonProperty jp = pd.getReadMethod().getDeclaredAnnotation(JsonProperty.class);
			//								if(jp != null ){
			//									return jp.value();
			//								}else{
			//									return pd.getName();
			//								}
			//							},
			//							// get value by invoking getter
			//							pd -> {
			//								try {
			//									return pd.getReadMethod().invoke(doc);
			//								} catch (IllegalAccessException | InvocationTargetException e) {
			//									return null;
			//								}
			//							}));
			//			//remove nulls since we don't want to overwrite
			objectMap.values().removeIf(Objects::isNull);
			if(UpdateMethod.PATCH.equals(updateMethod)) {
				//Convert Nested references to flattened person.info.email notation, prevents overwriting nested props
				objectMap = flattenMap(objectMap, null);
			}
			//loop through the Map and call the setter for that property on the Mongo Update object
			objectMap.forEach(update::set);
		}catch(Exception e){
			logger.error("Exception creating an Update object for entity {}",doc,e);
		}
		return update;
	}

	private Map<String,Object> flattenMap(Map<String,Object> nestedMap,final String path){
		logger.trace("Processing Path:{}",path);
		Map<String,Object> thisMap = new LinkedHashMap<>();
		for(Map.Entry<String,Object> entry : nestedMap.entrySet()){
			if(entry.getValue() == null){
				continue;
			}
			String thisPath;
			if(path == null){
				thisPath = entry.getKey();
			}else{
				thisPath = path + "." + entry.getKey();
			}
			logger.trace("Processing Key:{}, Current Path:{}",entry.getKey(),thisPath);
			if(entry.getValue() instanceof Map){
				logger.trace("Key:{} is a map",entry.getKey());
				thisMap.putAll(flattenMap((Map<String, Object>) entry.getValue(),thisPath));
			}else{
				logger.trace("Key:{} is a {}",entry.getKey(), entry.getValue().getClass());
				thisMap.put(thisPath,entry.getValue());
			}
		}
		return thisMap;
	}
}

