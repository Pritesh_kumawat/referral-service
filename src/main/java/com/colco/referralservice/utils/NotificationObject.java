package com.colco.referralservice.utils;

public class NotificationObject {

	private String to;

	private String priority="high";

	private Object data;


	private Object notification; 
	
	public Object getNotification() {
		return notification;
	}

	public void setNotification(Object notification) {
		this.notification = notification;
	}

	private String type; 

	/**
	 * 
	 * @param to
	 * @param priority
	 * @param data
	 */
	public NotificationObject(String to, Object data,String type,Object notification) {
		this.to = to;
		this.data = data;
		this.notification=notification;
		this.type=type;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	


	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	@Override
	public String toString() {
		return "NotificationObject: {to:" + to + ", priority:" + priority + ", data:" + data + ", notification:"
				+ notification + ", type:" + type + "}";
	}
	
	
}
