package com.colco.referralservice.utils;

import java.util.Collections;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.colco.referralservice.api.model.CreatedBy;
import com.colco.referralservice.controller.UsersApi;
import com.colco.referralservice.api.model.RabbitMQConstants;
import com.colco.usersgroups.api.model.User;
import com.colco.usersgroups.api.model.UserApiResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Component
public class NotificationHelper {

	org.slf4j.Logger logger = LoggerFactory.getLogger(NotificationHelper.class);

	@Autowired
	private UsersApi userApi;

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	private NotificationSender sender;

	// need to remove and modify unwanted code
	@Async
	public void notifyUserMain(Object refferalData, String opration, CreatedBy toUser, User fromUser, String anchorName,
			String accessToken) {

		try {
			logger.info("in notification");

			String postString = objectMapper.writeValueAsString(refferalData);
			if (RabbitMQConstants.REFFERAL_REWARD.equals(opration)) {
				ResponseEntity<UserApiResponse> userResult = userApi.usersGet(accessToken, null,
						Collections.singletonList(toUser.getUserId()), null, true);
				List<User> users = getUserList(userResult);
				logger.info("users : ,{}", users);
				ObjectNode object1 = objectMapper.createObjectNode();

				object1.put("title", "you got the reward");
				object1.put("body", toUser.getName() + " you got the rewards after compelting your assignment");
				ObjectNode dataNode = objectMapper.createObjectNode();
				dataNode.put("type", opration);
				dataNode.put("model", postString);
				dataNode.put("click_action", "FLUTTER_NOTIFICATION_CLICK");

				NotificationObject notification = new NotificationObject(users.get(0).getFcmToken(), dataNode, opration,
						object1);
				logger.info("notification Object {} ", notification);
				sender.sendNotification(notification);

			}
		} catch (Exception e) {
			logger.info("failed to send nofification due to {}", e);
		}
	}

//	public getTitleAndBody() {
//		
//	}

	public List<User> getUserList(ResponseEntity<UserApiResponse> userResult) throws Exception {
		UserApiResponse userRes = userResult.getBody();
		if (userRes.getResults() != null && !userRes.getResults().isEmpty()) {
			return userRes.getResults();
		}
		throw new Exception("no user Found");
	}

}
