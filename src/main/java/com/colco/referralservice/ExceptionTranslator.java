package com.colco.referralservice;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.colco.referralservice.api.model.ModelApiResponse;

@RestControllerAdvice
public class ExceptionTranslator extends ResponseEntityExceptionHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionTranslator.class);

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	@ResponseBody
	public ResponseEntity<?> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
		LOGGER.error("Exception mismatching argument", e);
		Class<?> type = e.getRequiredType();
		String message;
		if (type.isEnum()) {
			message = "The parameter " + e.getName() + " must have a value among : "
					+ StringUtils.arrayToDelimitedString(type.getEnumConstants(), ", ");
		} else {
			message = "The parameter " + e.getName() + " must be of type " + type.getTypeName();
		}
		return buildReponseEntity(message, HttpStatus.BAD_REQUEST);
	}

	private ResponseEntity<Object> buildReponseEntity(String message, HttpStatus responseStatus) {
		ModelApiResponse modelApiResponse = new ModelApiResponse();
		modelApiResponse.setStatusCode(responseStatus.value());
		List<String> messages = new ArrayList<>();
		messages.add(message);
		modelApiResponse.setErrorMessages(messages);
		return new ResponseEntity<Object>(modelApiResponse, responseStatus);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		LOGGER.error("MessageNotReadableException", ex);
		LOGGER.debug("HttpHeaders:{} HttpStatus:{} WebRequest:{}", headers, status, request);
		String error = "Malformed JSON request";
		return buildReponseEntity(error, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		LOGGER.error("handleMethodArgumentNotValid exception", ex);
		LOGGER.debug("HttpHeaders:{} HttpStatus:{} WebRequest:{}", headers, status, request);
		String error = "Malformed JSON request";
		return buildReponseEntity(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ResponseEntity<?> handleException(Exception ex) {
		LOGGER.error("Exception in Messaging Controller:", ex);
		return buildReponseEntity("Internal Server Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(NotFoundException.class)
	@ResponseBody
	public ResponseEntity<?> handleNotFoundException(NotFoundException ex) {
		LOGGER.error("NotFoundException in Api Controller:", ex);
		HttpStatus status = HttpStatus.resolve(ex.getCode());
		if (status == null) {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return buildReponseEntity(ex.getMessage(), status);
	}

	@ExceptionHandler(ApiException.class)
	@ResponseBody
	public ResponseEntity<?> handleApiException(ApiException ex) {
		LOGGER.error("handleApiException in Api Controller:", ex);
		HttpStatus status = HttpStatus.resolve(ex.getCode());
		if (status == null) {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return buildReponseEntity(ex.getMessage(), status);
	}

}
