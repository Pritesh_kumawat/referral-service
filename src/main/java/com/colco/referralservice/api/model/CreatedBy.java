package com.colco.referralservice.api.model;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatedBy {

	@JsonProperty("userId")
	private UUID userId = null;

	@JsonProperty("name")
	private String name = null;

	@JsonProperty("fileId")
	private UUID fileId = null;

	@JsonProperty("industries")
	private List<String> industries = null;

	@JsonProperty("offerings")
	private List<String> offerings = null;

	public List<String> getIndustries() {
		return industries;
	}

	public void setIndustries(List<String> industries) {
		this.industries = industries;
	}

	public List<String> getOfferings() {
		return offerings;
	}

	public void setOfferings(List<String> offerings) {
		this.offerings = offerings;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		if (userId != null) {
			builder.append("userId:");
			builder.append(userId);
			builder.append(", ");
		}
		if (name != null) {
			builder.append("name:");
			builder.append(name);
			builder.append(", ");
		}
		if (fileId != null) {
			builder.append("fileId:");
			builder.append(fileId);
			builder.append(", ");
		}
		if (industries != null) {
			builder.append("industries:");
			builder.append(industries);
			builder.append(", ");
		}
		if (offerings != null) {
			builder.append("offerings:");
			builder.append(offerings);
		}
		builder.append("}");
		return builder.toString();
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getFileId() {
		return fileId;
	}

	public void setFileId(UUID fileId) {
		this.fileId = fileId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fileId == null) ? 0 : fileId.hashCode());
		result = prime * result + ((industries == null) ? 0 : industries.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((offerings == null) ? 0 : offerings.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CreatedBy other = (CreatedBy) obj;
		if (fileId == null) {
			if (other.fileId != null)
				return false;
		} else if (!fileId.equals(other.fileId))
			return false;
		if (industries == null) {
			if (other.industries != null)
				return false;
		} else if (!industries.equals(other.industries))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (offerings == null) {
			if (other.offerings != null)
				return false;
		} else if (!offerings.equals(other.offerings))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
