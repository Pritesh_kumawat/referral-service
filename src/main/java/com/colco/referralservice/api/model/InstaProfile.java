package com.colco.referralservice.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InstaProfile {

    @JsonProperty("graphql")
    private GraphQL graphql;

    public GraphQL getGraphql() {
        return graphql;
    }

    public void setGraphql(GraphQL graphql) {
        this.graphql = graphql;
    }
}
