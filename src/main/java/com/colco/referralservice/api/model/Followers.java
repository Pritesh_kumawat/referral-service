package com.colco.referralservice.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Followers {

    @JsonProperty("count")
    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
