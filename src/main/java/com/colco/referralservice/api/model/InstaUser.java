package com.colco.referralservice.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InstaUser {

    @JsonProperty("username")
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("edge_followed_by")
    private Followers edge_followed_by;

    public Followers getEdge_followed_by() {
        return edge_followed_by;
    }

    public void setEdge_followed_by(Followers edge_followed_by) {
        this.edge_followed_by = edge_followed_by;
    }
}
