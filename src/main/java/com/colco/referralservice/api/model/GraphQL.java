package com.colco.referralservice.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GraphQL {

    @JsonProperty("user")
    private InstaUser user;

    public InstaUser getUser() {
        return user;
    }

    public void setUser(InstaUser user) {
        this.user = user;
    }
}
