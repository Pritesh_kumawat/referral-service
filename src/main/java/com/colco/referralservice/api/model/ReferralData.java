package com.colco.referralservice.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.OffsetDateTime;
import java.util.UUID;


public class ReferralData {

	@Id
	@JsonProperty("id")
	private String id;// referralId

	@JsonProperty("instaUsername")
	private String instaUsername;

	@JsonProperty("createdBy")
	private CreatedBy createdBy;

	@JsonProperty("followerCount")
	private Integer followerCount;

	@JsonProperty("referralCount")
	private Integer referralCount;

	@JsonProperty("percentage")
	private Integer percentage;

	@JsonProperty("fcmToken")
	private String fcmToken;

	@JsonProperty("createdDate")
	private OffsetDateTime createdDate;

	@JsonProperty("modifiedDate")
	private OffsetDateTime modifiedDate;

	public OffsetDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(OffsetDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public OffsetDateTime getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(OffsetDateTime modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public CreatedBy getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(CreatedBy createdBy) {
		this.createdBy = createdBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInstaUsername() {
		return instaUsername;
	}

	public void setInstaUsername(String instaUsername) {
		this.instaUsername = instaUsername;
	}

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}

	public Integer getPercentage() {
		return percentage;
	}

	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}

	public Integer getFollowerCount() {
		return followerCount;
	}

	public void setFollowerCount(Integer followerCount) {
		this.followerCount = followerCount;
	}

	public Integer getReferralCount() {
		return referralCount;
	}

	public void setReferralCount(Integer referralCount) {
		this.referralCount = referralCount;
	}
}
