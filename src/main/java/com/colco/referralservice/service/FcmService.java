package com.colco.referralservice.service;

import com.colco.referralservice.api.model.PushNotificationRequest;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class FcmService {

    private Logger logger = LoggerFactory.getLogger(FcmService.class);

    @Value("${app.firebase-configuration-file}")
    private String firebaseConfigPath;

    @PostConstruct
    public void initialize() {
        try {
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(new ClassPathResource(firebaseConfigPath).getInputStream())).build();
            if (FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
                logger.info("Firebase application has been initialized");
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public String get(PushNotificationRequest pushNotificationRequest){
        Message message = Message.builder()
                .setToken(pushNotificationRequest.getToken())
                .setNotification(new Notification(pushNotificationRequest.getTitle(), pushNotificationRequest.getMessage()))
                .putData("content", pushNotificationRequest.getTitle())
                .putData("body", pushNotificationRequest.getMessage())
                .build();

        String response = null;
        try {
            response = FirebaseMessaging.getInstance().send(message);
        } catch (FirebaseMessagingException e) {
            logger.error("Fail to send firebase notification", e);
        }

        return response;
    }
}