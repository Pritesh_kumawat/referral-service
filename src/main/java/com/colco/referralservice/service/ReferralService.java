package com.colco.referralservice.service;

import com.colco.referralservice.ApiException;
import com.colco.referralservice.data.model.ReferralData;
import com.colco.usersgroups.api.model.User;

public interface ReferralService {

	public com.colco.referralservice.api.model.ReferralData createReferralData(String username,User user);

	public com.colco.referralservice.api.model.ReferralData referralCodeUsed(String referralCode,User user);

	public com.colco.referralservice.api.model.ReferralData getReferralCodeUsed(String referralCode,User user) throws ApiException;
}
