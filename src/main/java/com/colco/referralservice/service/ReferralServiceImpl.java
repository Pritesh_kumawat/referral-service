package com.colco.referralservice.service;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Random;

import com.colco.referralservice.config.CodeConfig;
import com.colco.referralservice.mapper.ModelConverter;
import com.colco.referralservice.utils.NotificationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.colco.referralservice.ApiException;
import com.colco.referralservice.api.model.InstaProfile;
import com.colco.referralservice.data.model.ReferralData;
import com.colco.referralservice.utils.CommonUttils;
import com.colco.usersgroups.api.model.User;

@Service
public class ReferralServiceImpl implements ReferralService {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	NotificationHelper notificationHelper;

	@Autowired
	ModelConverter modelConverter;

	private static final Random RND = new Random(System.currentTimeMillis());

	private static final Logger logger = LoggerFactory.getLogger(ReferralServiceImpl.class);

	public Query formQueryByReferralCode(String Id) {
		return Query.query(Criteria.where("_id").is(Id));

	}

	public Query formQueryByUsername(String username) {
		return Query.query(Criteria.where("instaUsername").is(username));
	}

	public InstaProfile getInstaUserDetails(String name) {

		logger.info("In getInstaUserDetails(String name)");

		String url = "https://www.instagram.com/" + name + "/?__a=1";
		ResponseEntity<InstaProfile> objectResponseEntity = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<InstaProfile>() {
				});
		InstaProfile instaProfile = objectResponseEntity.getBody();
		return instaProfile;
	}

	//random code generator
	public String generate() {
		logger.info("In generate()");
		CodeConfig config = new CodeConfig(null, null, null, null, null);
		StringBuilder sb = new StringBuilder();
		char[] chars = config.getCharset().toCharArray();
		char[] pattern = config.getPattern().toCharArray();

		if (config.getPrefix() != null) {
			sb.append(config.getPrefix());
		}

		for (int i = 0; i < pattern.length; i++) {
			if (pattern[i] == CodeConfig.PATTERN_PLACEHOLDER) {
				sb.append(chars[RND.nextInt(chars.length)]);
			} else {
				sb.append(pattern[i]);
			}
		}

		if (config.getPostfix() != null) {
			sb.append(config.getPostfix());
		}

		return sb.toString();
	}

	@Override
	public com.colco.referralservice.api.model.ReferralData createReferralData(String instaUsername, User user) {

		logger.info("In function createReferralData(String instaUsername, User user)");

		// check if instagram account is already used
		if (mongoTemplate.exists(formQueryByUsername(instaUsername), ReferralData.class)) {
			ReferralData referralData = mongoTemplate.findOne(formQueryByUsername(instaUsername), ReferralData.class);
			return modelConverter.dataToapi(referralData);
		}
		InstaProfile instaProfile = getInstaUserDetails(instaUsername);
		ReferralData referralData1 = new ReferralData();
		referralData1.setCreatedBy(CommonUttils.getCreatedBy(user));
		referralData1.setInstaUsername(instaProfile.getGraphql().getUser().getUsername());
		referralData1.setFollowerCount(instaProfile.getGraphql().getUser().getEdge_followed_by().getCount());
		if(user.getRoles().toString().equals("n_user")) {
			String id = checkForDuplicate(user.getUserDetails().getFirstName()	, generate());
			referralData1.setId(id);
		}
		else{
			String id = checkForDuplicate(user.getBusinessDetails().getLegalName(), generate());
			referralData1.setId(id);
		}
		int percentage = (20 * referralData1.getFollowerCount()) / 100;
		referralData1.setPercentage(percentage);
		referralData1.setReferralCount(0);
		referralData1.setCreatedDate(OffsetDateTime.now(ZoneId.of("UTC")));
		mongoTemplate.save(referralData1);

		logger.info("successfully saved in MongoDb");

		return modelConverter.dataToapi(referralData1);

	}

	@Override
	public com.colco.referralservice.api.model.ReferralData referralCodeUsed(String referralCode, User user) {

		logger.info("In function referralCodeUsed(String referralCode, User user)");

		ReferralData referralData = mongoTemplate.findOne(formQueryByReferralCode(referralCode), ReferralData.class);
		referralData.setReferralCount(referralData.getReferralCount() + 1);
		referralData.setModifiedDate(OffsetDateTime.now(ZoneId.of("UTC")));
		if (referralData.getReferralCount() == referralData.getPercentage() && referralData.getFollowerCount() > 100) {
			notificationHelper.notifyUserMain(referralData, "referrals_reward", referralData.getCreatedBy(), user, "anchorName", "" );
		}
		mongoTemplate.save(referralData);
		logger.info("successfully saved in MongoDb");

		return modelConverter.dataToapi(referralData);
	}

	//check for duplicate referral code
	public String checkForDuplicate(String username, String randomCode) {
		boolean check = true;
		while (check) {
			if (mongoTemplate.exists(formQueryByReferralCode(username + "_" + randomCode), ReferralData.class)) {
				randomCode = generate();
				continue;
			} else {
				break;
			}
		}
		return username + "_" + randomCode;
	}

	public com.colco.referralservice.api.model.ReferralData getReferralCodeUsed(String referralCode, User user) throws ApiException {
		logger.info("In function getReferralCodeUsed(String referralCode, User user) ");

		ReferralData referralData = mongoTemplate.findOne(formQueryByReferralCode(referralCode), ReferralData.class);
		if (referralData != null) {
			return modelConverter.dataToapi(referralData);
		} else {
			throw new ApiException(HttpStatus.NOT_FOUND.value(), "resource not found");
		}

	}

}
