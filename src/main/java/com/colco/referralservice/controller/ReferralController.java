package com.colco.referralservice.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.colco.referralservice.ApiException;
import com.colco.referralservice.service.ReferralService;
import com.colco.usersgroups.api.model.User;

@RestController
public class ReferralController {

	@Autowired
	UsersApi usersApi;

	@Autowired
	HttpServletRequest request;

	@Autowired
	ReferralService referralService;

	@PostMapping(value = "/referrals/{username}", produces = { "application/json" })
	public ResponseEntity<com.colco.referralservice.api.model.ReferralData> createReferralData(@RequestParam String username) {
		String accessToken = request.getHeader(HttpHeaders.AUTHORIZATION);
		User user = usersApi.getUserMe(accessToken).getBody().getResults().get(0);
		com.colco.referralservice.api.model.ReferralData referralData = referralService.createReferralData(username, user);
		return new ResponseEntity<>(referralData, HttpStatus.CREATED);
	}

	@GetMapping(value = "/referrals/{referralCode}", produces = { "application/json" })
	public ResponseEntity<com.colco.referralservice.api.model.ReferralData> getReferralCodeUsed(@PathVariable(value = "referralCode") String refreralCode)
			throws ApiException {
		String accessToken = request.getHeader(HttpHeaders.AUTHORIZATION);
		User user = usersApi.getUserMe(accessToken).getBody().getResults().get(0);
		com.colco.referralservice.api.model.ReferralData referralData = referralService.getReferralCodeUsed(refreralCode, user);
		return new ResponseEntity<>(referralData, HttpStatus.OK);
	}

	@PutMapping(value = "/referrals/{referralCode}", produces = { "application/json" })
	public ResponseEntity<com.colco.referralservice.api.model.ReferralData> referralCodeUsed(@PathVariable(value = "referralCode") String referralCode) {
		String accessToken = request.getHeader(HttpHeaders.AUTHORIZATION);
		User user = usersApi.getUserMe(accessToken).getBody().getResults().get(0);
		com.colco.referralservice.api.model.ReferralData referralData = referralService.referralCodeUsed(referralCode, user);
		return new ResponseEntity<>(referralData, HttpStatus.OK);
	}
}
