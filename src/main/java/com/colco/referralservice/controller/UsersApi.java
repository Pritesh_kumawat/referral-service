package com.colco.referralservice.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.colco.usersgroups.api.model.GroupApiResponse;
import com.colco.usersgroups.api.model.UserApiResponse;

import io.swagger.annotations.ApiParam;

@FeignClient(name = "user-gate-way", url = "${user.service.url}")
public interface UsersApi {

	@RequestMapping(method = RequestMethod.GET, value = "/user/me", produces = "application/json")
	ResponseEntity<UserApiResponse> getUserMe(@RequestHeader(HttpHeaders.AUTHORIZATION) String token);

	@RequestMapping(method = RequestMethod.POST, value = "/groups", produces = "application/json")
	ResponseEntity<GroupApiResponse> addGroups(@RequestBody com.colco.usersgroups.api.model.Group group,
			@RequestHeader(HttpHeaders.AUTHORIZATION) String token);

	@RequestMapping(method = RequestMethod.GET, value = "/groups", produces = "application/json")
	ResponseEntity<GroupApiResponse> groupsGet(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
			@RequestParam(value = "groupIds", required = false) List<UUID> groupIds,
			@RequestParam(value = "userId", required = false) UUID userId,
			@RequestParam(value = "applicationCode", required = false) String applicationCode,
			@RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset,
			@RequestParam(value = "limit", required = false, defaultValue = "200") Integer limit);

	@RequestMapping(method = RequestMethod.GET, value = "/users", produces = "application/json")
	ResponseEntity<UserApiResponse> usersGet(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
			@ApiParam(value = "GroupId(s) of the group(s) that the user is a member of.") @Valid @RequestParam(value = "groupId", required = false) List<UUID> groupId,
			@ApiParam(value = "UserId(s) of the user(s) to retrieve") @Valid @RequestParam(value = "userId", required = false) List<UUID> userId,
			@ApiParam(value = "Search field to find user") @Valid @RequestParam(value = "search", required = false) String search,
			@RequestParam(value = "isVerified", required = false, defaultValue = "true") Boolean isVerified);

}
