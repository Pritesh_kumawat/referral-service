package com.colco.referralservice.mapper;

import com.colco.referralservice.data.model.ReferralData;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ModelConverter {

    ReferralData apiToData(com.colco.referralservice.api.model.ReferralData referralData);
    com.colco.referralservice.api.model.ReferralData dataToapi(ReferralData referralData);
}
