package com.colco.referralservice;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-03T09:23:34.436+05:30[Asia/Calcutta]")
public class ApiException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4379920855163429478L;
	private int code;

	public ApiException(int code, String msg) {
		super(msg);
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

}
